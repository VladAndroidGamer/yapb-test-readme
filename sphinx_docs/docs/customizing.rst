******************************
Customization
******************************

Chat customization
================================
You can create a new chat base for your language or change the existing one.
It is located in the folder: ``addons/yapb/conf/lang``
To do this, create a file ``**_chat.cfg`` or open an existing one.

Bots can write chat messages depending on the situation: planting a bomb, killing an enemy, attacking teammates, etc.

Placeholders
--------------------------

* ``%v`` - inserts the nickname of the killed player
* ``%f`` - inserts the nickname of the team leader
* ``%t`` - inserts the nickname of a random teammate
* ``%m`` - inserts the name of the current map
* ``%s`` - inserts the nickname of the player who wrote the message to which the bot replied or the killer's nickname if the bot wrote a message from [DEADCHAT]

Action triggers
--------------------------
``[BOMBPLANT]`` - Sets a list of messages that bots will write after planting a bomb.

``[KILLED]`` - Sets a list of messages for bots that will write after killing an enemy.
Use the placeholder ``%v`` to write the nickname of the killed enemy.
Example::

	[KILLED]
	You're dead %v!

::

When the bot kills the enemy with the nickname "John Smith", he will write "You're dead John Smith!" using the line shown in the example.

``[WELCOME]`` - Sets the list of messages that the bot will write when it connects to server.

``[DEADCHAT]`` - Sets the list of messages that the bot will write when it is dead or is in spectator mode

.. note:: Minimum number is 9! If you write less than 9 messages for this trigger, yapb will crash!

``[REPLIES]`` - Sets the list of messages that the bot will write in answer to another bot if it has an answer to the specified word
To set a word to which the answer will be, you need to set a key to specified words separated by commas
Example::

	[REPLIES]
	@KEY "WORD", "ANOTHER WORD"
	This is the answer to the given words.
	This is another answer to the given words.
	
	@KEY "KEYWORD"
	This is the answer to new key word.
	
::

This is how it will look in the game::

	John Smith: Bla bla bla word
	Ricardo Milos: This is answer to the given words.
	
	Keanu Reeves: Bla bla bla another word bla bla...
	Tommy Vercetti: This is another answer to the given words.
	
	Ryan Gosling: Answer me a new keyword.
	Soap MacTavish: This is the answer to new key word.
	
::

Bots can use these replies at random. 

.. warning:: Please note that the key words in the [REPLIES] trigger must be written in capital letters! In messages, they can be written in any format.

``[UNKNOWN]`` - Sets the list of messages that the bot will write in answer to another bot if it has no response in the [REPLIES] trigger

VoiceChat customization
================================
YaPB supports voice chat as well, as zBot.
All paths for yapb voice chat audio files are in the file: ``chatter.cfg`` which located in the folder ``addons/yapb/conf``.

``RewritePath`` sets the folder where the voice chat audio files are located. By default it is ``sound/radio/bot``

``Event Radio_***`` sets the name of the sound files that will be played when the bot speaks a specific radio commands.
For example ``Event Radio_CoverMe`` will contain the sounds that the bot will speak for the command "Cover me!"
You can comment out these lines if you want the bot to use standard radio commands.

``Event Chatter_***`` sets the names of sound files that the bot will speak with a non-standard radio commands.
For example ``Event Chatter_HearSomething`` will contain the sounds that the bot will speak when it hears an enemy.

How this file should look like::

	RewritePath sound/radio/bot
	
	Event Radio_Affirmative = ("affirmative", "no2", "roger_that", "me_too", "ill_come_with_you", "ill_go_with_you", "ill_go_too", "i_got_your_back", "i_got_your_back2", "im_with_you", "im_with_you", "sounds_like_a_plan", "good_idea");
	Event Radio_EnemySpotted = ("one_guy", "two_of_them", "theyre_all_over_the_place2", "the_actions_hot_here", "its_a_party");
	Event Radio_NeedBackup = ("taking_fire_need_assistance2", "i_could_use_some_help", "i_could_use_some_help_over_here", "help", "need_help", "need_help2", "im_in_trouble");
	// Event Radio_ShesGonnaBlow = ("");
	
	Event Chatter_GoingToPlantBomb = ("im_gonna_go_plant", "im_gonna_go_plant_the_bomb");
	Event Chatter_RescuingHostages = ("the_hostages_are_with_me", "taking_the_hostages_to_safety", "ive_got_the_hostages", "i_have_the_hostages");
	Event Chatter_GoingToCamp = ("im_going_to_camp");
	Event Chatter_HearSomething = ("hang_on_i_heard_something", "i_hear_something", "i_heard_them", "i_heard_something_over_there");
	Event Chatter_TeamKill = ("what_happened", "noo", "oh_my_god", "oh_man", "oh_no_sad", "what_have_you_done"); 
	
::