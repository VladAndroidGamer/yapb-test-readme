## conf.py

# Set the project name, version and master file for documentation
html_title = 'Official YaPB Documentation'
master_doc = 'index'
project = u'Official YaPB Documentation'
version = 'latest'
release = 'latest'
htmlhelp_basename = 'Official YaPB Documentation'