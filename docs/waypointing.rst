*********************
Waypointing
*********************

.. include:: .fontstyles.rst

Brief information
==========================

Waypoints, what are they?
--------------------------

Unlike humans, bots cannot see a map and analyze what they see. If you see a building with a door, you can walk straight to the door, open it and enter the building.
Bots cannot do this without help! They can see and fight enemies or react if they are being attacked. These ways of behaviour work without any external help. But in order to find their way around the map and to safely navigate through all ways and passages, they do need some help. They need something that tells them where they can go and where they can't. They need something that shows them where a ladder is located or where the mission goal (escape zone/hostages/bomb spot) is. This is done by means of waypoints. You can imagine waypoints a bit like those flags on a ski run. Each waypoint marks a point where bots can go. If two of them are connected with each other, a bot can go from one point to the other and back. So what you do when you waypoint a map is basically place a whole net of points in the map and connect them in a way that bots can proceed from one point to the other. All points must be placed in areas that are accessible for players, and if you want your bots to navigate smoothly and safely, you must also keep an eye on the connections. If connections go through walls or over a deep ravine, your bots will bump into walls or fall to their death.
There are several waypoint types that can be used to indicate map goals, rescue zones, nice camp spots, ladders etc. There are different types of connections too, one-way or two-way connections and jump connections that will make a bot jump from point A to B instead of walking or running there. We will come back to this later.

Besides, you don't have to worry about every little detail. The editor that comes with this bot version will do lots of the work for you, and besides, it's graphical and easy to use (no programming/coding skills or anything required). You may very well discover that making waypoints can be fun, especially when you see bots roam through the entire map without problems - and you made it possible!.

.. Note:: Since YaPB 2.10 version, a new waypoint format named **Graph** has been added. Thanks to this, points limit was increased to 2048, directions of camp points can be set up and down, not just left and right and reduced the size of waypoint files. YaPB also continues to support the old **PWF** format.


What do waypoints look like in the game?
----------------------------------------

When you are playing a normal game on a waypointed map, the waypoints will of course be invisible so that they don't distract or annoy you in any way.
When waypointing is activated (see: How can I access the waypoint editor?, Below for instructions on how to do that), you will see waypoints as vertical bars about as high as a standing player. The colour of normal waypoints is green, but you may also see waypoints in white, purple, red, blue and cyan. These colours indicate special waypoints, some of which have already been mentioned in the last paragraph. If you see waypoints that are much smaller than the other ones, they are crouch waypoints. They will force bots to crouch when approaching them. Such waypoints are needed to lead bots through vents or any other low and narrow passages.
Connections between waypoints are marked as horizontal lines leading from the centre of one waypoint to the other. They, too, exist in different colours. You may see yellow, white, red and teal lines. Don't worry if all these different colours sound confusing right now - it's actually very easy, but of course it helps a lot if you see some waypoints on the screen.
Now you know what waypoints and connections are, what they are for and what they look like, you may want to enter the editing mode and see for yourself.


How can I access the waypoint editor?
-------------------------------------

The waypoint editor is not a separate program, it is included in the bot dll (or .so, if you are using linux).To open it, create a ``LAN\Listen Server`` game, select the map you want to waypoint and start the game as usual. As soon as you are in the map, you can activate the editing mode from the console by typing ``yb graphmenu`` or if you have bound a key for it, simply by pressing that key.


Graph console commands Summary
-------------------------------

The following Graph commands are available (note these ARE case sensitive):
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g on``               | Turns on displaying of nodes.                                                                                                  |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g off``              | Turns off displaying of nodes.                                                                                                 |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g on auto``          | Turns on auto nodes placement setting (see below).                                                                             |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g off auto``         | Turns off auto nodes placement setting (see below).                                                                            |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g on models``        | Turns on the player models rendering on spawn points.                                                                          |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g off models``       | Turns off the player models rendering on spawn points.                                                                         | 
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g on noclip``        | Turns on nodes editing with noclip cheat. This allows you to fly and you don't collide with walls.                             |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g off noclip``       | Turns off nodes editing with noclip cheat.                                                                                     |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g add``              | Adds a node at the current player location. A Menu will pop up where you have to select the different types of nodes.          |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g addbasic``         | Adds basic nodes on map, like spawn points, goals and ladders.                                                                 |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g cache``            | Remember the nearest to player node.                                                                                           |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g clean``            | Cleans useless path connections from all or single node.                                                                       |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g delete``           | Deletes the node nearest to the player (see below).                                                                            |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g erase``            | Removes the graph and bot experience files from hard drive.                                                                    |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g flags``            | Allows you to manually add/remove Flags to a node.                                                                             |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g setradius x``      | Manually sets the Wayzone Radius for this node to value x.                                                                     |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g teleport x``       | Teleport hostile to specified in value x node.                                                                                 |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g stats``            | Shows the number of different nodes you did already set.                                                                       |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g check``            | Checks if all node connections are valid.                                                                                      |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g load``             | Loads the nodes from a graph file (see below).                                                                                 |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g save``             | Saves the current nodes to a file (see below).                                                                                 |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g save nocheck``     | Saves the current nodes to a file without validating.                                                                          |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g upload``           | Uploads created graph file to graph database.                                                                                  |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb graphmenu``          | Show the graph editor menu.                                                                                                    |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g path_set_autopath``| Opens menu for setting autopath maximum distance.                                                                              |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g path_create``      | Opens menu for path creation.                                                                                                  |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g path_delete``      | Delete path from cached (or faced) to nearest node.                                                                            |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g path_create_in``   | Creating incoming path connection from cached (faced) to nearest node.                                                         |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g path_create_out``  | creating outgoing path connection from cached (faced) to nearest node.                                                         |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g path_create_both`` | Creating both-ways path connection from cached (faced) to nearest node.                                                        |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+
   | ``yb g iterate_camp``     | Allows to go through all camp points on map.                                                                                   |
   +---------------------------+--------------------------------------------------------------------------------------------------------------------------------+

To use the graph commands, you will have to use the console. Use the ``~`` key to bring down the console. Enter the console commands that you wish, then use the ``~`` key again to return to the game.


Adding waypoints
---------------------------------

Adding a waypoint is really easy. Just walk to the position where you want a waypoint to be inserted, bring up your waypoint menu:

.. figure:: images/waypoint_menu_page1.png\ .. figure:: images/waypoint_menu_page2.png
    :align: center

    Waypoint Menu (Page 1).
	
To add a waypoint, simply select ``5. Add waypoint``. A new menu will appear, the "\ :green:`Waypoint Type`\ " menu All waypoint types described below can be added by using this menu.

.. figure:: images/waypoint_type_menu.png
    :align: center

    Waypoint Type Menu.
	
Once you have selected a waypoint type from the "Waypoint Type" menu, you will hear a sound, and the selected waypoint will appear in the map, at the exact position where you stood when you pressed the key.

.. Note:: If you are standing while selecting a waypoint, a standing waypoint will be inserted. All bots will run or walk towards this waypoint normally. If you want bots to crouch when approaching a particular position, crouch down when inserting the waypoint. You will notice that the waypoint you just added is only about half as high as normally. As you inserted it when you were crouched down, it automatically carries a "\ :pink:`Crouch`\ " flag (see: Types Of Waypoints). Bots will now crouch automatically when trying to reach this waypoint.

Now that you know the basic method used to add a waypoint, let's have a closer look at the waypoint types that exist.


Types of Waypoints
-----------------------------------

Normal waypoints
~~~~~~~~~~~~~~~~~~

:green:`Normal` waypoints are the points you need in order to make bots walk through the map. They are used for navigation only and will not trigger any particular behaviour. You can add a :green:`Normal` waypoint by selecting ``1. Normal`` from the "\ :green:`Waypoint Type`\ " menu. :green:`The colour of Normal waypoints is green`, as you can see in the picture below.

.. figure:: images/normal_point.png
    :align: center

    Normal Waypoint.
	
Terrorist Important waypoints
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This type of waypoints can be navigated just as a :green:`Normal` waypoint by all bots, but it has one additional function. It marks strategically important points for a Terrorist team. Adding a :red:`Terrorist Important` point in a room will tell Terrorist bots to go to the room and check it frequently. You can add this type of waypoint by selecting ``2. Terrorist Important`` from the "\ :green:`Waypoint Type`\ " menu. :red:`The colour of Terrorist Important waypoints is green with red head`, as you can see in the picture below.

.. figure:: images/terrorist_important_point.png
    :align: center

    Terrorist Important Waypoint.
	
.. important:: The use of :red:`Terrorist Important` points depends on the map type! Wherever the Terrorist team is the "defending" team (i.e. on As_ Cs_ type maps), :red:`Terrorist Important` points should be placed at key positions around the hostage area or VIP escape zone. For example, if the hostages are inside a building, :red:`Terrorist Important` points should be added behind each entrance to the building. Doing so will make the Terrorists check all entrances frequently and guard them. Do not place :red:`Terrorist Important` points far away on the other side of the map. After all, you don't want the Terrorists to abandon the hostages and rush planlessly through the map, now, do you? With the VIP escape zone, the same strategy applies, Make Terrorists guard the key routes to the escape zone by using :red:`Terrorist Important` waypoints. You :redbold:`DON'T` need to place :red:`Terrorist Important` points directly at the hostages. Terrorists will check on hostages anyway. On maps where the Terrorist team is "offensive" (i.e. De_ and Es_ type maps), :red:`Terrorist Important` waypoints should not be overused. The "offensive" team will try to reach the map goal waypoint anyway. The only useful function you can use important waypoints for is to make particular routes more attractive for the bots. For example, if there is a longer and more complicated, but safer and more surprising route to the map goal, bots may tend to underuse it a little. In such cases, placing one or two :red:`Terrorist Important` waypoints along this route can help.


Counter-Terrorist Important waypoints
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The function of this waypoint type is exactly the same as the :red:`Terrorist Important` waypoint described above. The only difference is that a :blue:`Counter-Terrorists Important` waypoint obviously marks strategically important places for the :blue:`Counter-Terrorist (CT)` team. You can add this type of waypoint by selecting ``3. Counter-Terrorist Important`` from the "\ :green:`Waypoint Type`\ " menu. :blue:`The colour of Counter-Terrorist Important waypoints is green with blue head`, as you can see in the picture below.

.. figure:: images/counter_terrorist_important_point.png
    :align: center

    Counter-Terrorist Important Waypoint.
	
.. important:: As with the other team specific waypoints, :blue:`Counter-Terrorist Important` waypoints should also be placed according to the map type. On maps where the Counter-Terrorist team is forced to move out and reach a certain goal - either hostages to rescue or a VIP escape zone to reach safely. :blue:`Counter-Terrorist Important` points can be useful to make a particular route more attractive. You :redbold:`DON'T` need to place :blue:`Counter-Terrorist Important` points near a map goal (hostages on CS_ maps, VIP escape zone(s) on As_ maps), Counter-Terrorist bots will go there anyway. It's the most important point for them, and adding several other important waypoints right next to it doesn't yield any benefit. On maps where the Counter-Terrosist team is in a defensive role (i.e. on De_ maps and Es_ maps), place :blue:`Counter-Terrorist Important` points at key positions around the bomb/escape zone(s) in order to make Counter-Terrorist bots defend all possible routes to the Terrorists map goal.


Ladder waypoints
~~~~~~~~~~~~~~~~~~

:chocolate:`Ladder` waypoints are only used for waypointing ladders, as you possibly guessed. To enable your bots to use a ladder, simply walk up to the ladder until you get "stuck" on it (you will see your crosshair grow wider once you are on the ladder). Now place one :chocolate:`Ladder` waypoint at the bottom of the ladder. Then climb up the ladder until you are almost completely over the edge. Place a second waypoint here and make sure that the two ladder waypoints are connected, (This should have happened automatically if the :chocolate:`Ladder` waypoints aren't too far away from each other, if not you can create a connection manually), That's all! You can add this type of waypoint by selecting ``1. Normal`` from the "\ :green:`Waypoint Type`\ " menu, it will automatically turn into a ladder waypoint if you are standing on ladder. Or choose ``4. Block with hostage / Ladder`` from the "\ :green:`Waypoint Type`\ " menu if it's a Hostage Rescue (CS_) scenario map so that bots don't miss the hostages when going up on ladders. :chocolate:`The colour of Ladder waypoints is brown`, as you can see in the picture below.

.. figure:: images/ladder_point.png
	:align: center
	
	Ladder waypoint
	
Some general hints and notes concerning ladder waypoints:
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

#. Waypoint ladders AFTER you waypointed the areas above and below them! If you waypoint ladders first, all waypoints in reach of a ladder waypoint will be connected with it and have their radius reduced to zero automatically!
#. It doesn't matter whether you place the top or the bottom :chocolate:`Ladder` waypoint first.
#. If the ladder is very long, you can place additional :chocolate:`Ladder` waypoints between the bottom and the top end.
#. The bottom waypoint will automatically get connected with the nearest waypoint, independent of current AutoPath Max Distance settings.
#. The top waypoint will usually get a connection towards it automatically, but you will have to add a connection leading away from it manually.
#. :chocolate:`Ladder` waypoints will always have a radius of zero, and this shouldn't be changed!


Rescue waypoints
~~~~~~~~~~~~~~~~~~

:white:`Rescue` waypoints are only needed on Cs_ type maps (hostage rescue scenarios). They mark the zone where the Counter-Terrosist team must bring the hostages, the rescue zone. Place one of these waypoint inside each rescue zone there is. If there is only one, you only need one :white:`Rescue` waypoint. Placing more points in one rescue zone is unnecessary bulk and will rather cause problems than improve anything.
A Counter-Terrorist bot that has succeeded in "activating" the hostages will determine the position of the nearest rescue point and lead the hostages there. When the bot has reached the rescue point, it will check if the hostages are really rescued and after max time about 5 seconds turn back to return to combat. Badly placed rescue points may lead to bots turning around before the hostages have really reached the rescue zone. As a consequence, the hostages will be left standing a few inches away from the rescue zone while the bot considers its mission as completed and turns back to fight, ignoring the deserted hostages. That's why you are advised to place a rescue waypoint well inside a rescue zone, not at its edges!
In the editor, rescue points will be displayed in bright white (see below). Their radius is set to zero by default and shouldn't be changed. All bots can use this waypoint type for :green:`Normal` navigation as well. You can add this type of waypoint by selecting ``5. Rescue Zone`` from the "\ :green:`Waypoint Type`\ " menu. :white:`The colour of Rescue waypoints is white`, as you can see in the picture below.

.. figure:: images/hostage_rescue_point.png
	:align: center
	
	Hostage Rescue waypoint
	

Camp waypoints
~~~~~~~~~~~~~~~~~~

As the name suggests, Camp waypoints are used to mark good sniper spots. They can be navigated by all bots. However, whether a bot may camp there or not is determined by the flag you can add to the camp waypoint. You can make Camp waypoints team specific or leave them "open" to any team. The colour of Normal Camp waypoints is cyan. Terrorist specific camp waypoints have cyan color with red head, Counter-Terrorist specific is cyan with blue head, as you can see in the pictures below.

.. figure:: images/camp_points.png
	:align: center
	
	From left to right. Sniper, Counter-Terrorist Specific, Normal and Terrorist Specific camp waypoints.
	
Although there are two entries in the "\ :green:`Waypoint Type`\ " menu ("\ :green:`Camping`\ " and "\ :green:`Camp end`\ "), the :cyan:`Camp` waypoint is in fact only one point. However, it carries two "markers" that tell a camping bot where to look while camping. When you are camping yourself, you will monitor a certain area. If you wanted to define this area, you could describe it as an angle. This angle would be specified by two lines going out from your position: One that marks the left edge and another one for the right edge. The monitored area would be between these two lines. The mentioned "markers" fulfill exactly this function. They are displayed as more or less horizontal beams going out from the top of a :cyan:`Camp` waypoint. :red:`The colour of Camp markers is red`, as you can see in the picture below.

.. figure:: images/camp_directions.png
	:align: center
	
	A crouched normal Camp waypoint with Camp start and Camp end markers (directions)

When a bot approaches the depicted :cyan:`Camp` waypoint, it will turn to face the direction of the Camp start marker first. Then it will scan the area between this marker and the Camp end marker by changing every few seconds the direction it is facing from one to the other. An enemy moving outside the two markers may escape the bot's attention, unless it hears the enemy coming. In the piture above, both markers are pointing to the same height. However, you can also specify different heights for each marker. This is very useful for making bots monitor a ramp, a slope, a stairway or other uneven surfaces.
So far, so good. But how to set a working camp waypoint? Follow these steps:
#. Go to the exact position where you want bots to camp (of course, a dark corner or similar locations are best suited for camping, but whom do I tell this?)
#. If you want bots to stand while they are camping, remain standing upright. If you want them to crouch while camping (more precise aiming!), crouch yourself while adding the point
#. Point your crosshair at the exact direction and height where you want your bots to start looking.
#. Bring up the "\ :green:`Waypoint Type`\ " menu and select ``6. Camping``. The :cyan:`Camp` waypoint itself will now be placed at your current position, and you will see the two marker beams going out from it. The Camp start marker will already be pointed at the direction you specified, the Camp end marker will still need some adjustment.
#. Now point your crosshair at the exact direction and height where you want your bots to end their monitoring.
#. Once again, open the "\ :green:`Waypoint Type`\ " menu, but now select "7. Camp end". You will see that the Camp end marker will now be pointed at the direction you specified.

That's it! Unless you want to make your :cyan:`Camp` waypoint team specific or add another flag (see: Waypoint Flags section), you are done! In fact, it sounds much more complicated than it actually is.

Some quick notes and hints about Camp waypoints:
"""""""""""""""""""""""""""""""""""""""""""""""""

#. You can alter Camp start and Camp end markers as often as you want. As soon as you are near an existing :cyan:`Camp` waypoint (i.e. as soon as its waypoint stats are shown in the upper left corner of your HUD), Bring up the "\ :green:`Waypoint Type`\ " menu and selecting ``6. Camping`` or ``7. Camp end`` will :redbold:`NOT` add a new waypoint. Instead, it will readjust the Camp start and/or Camp end marker(s) of the nearby :cyan:`Camp` waypoint to the new direction you specified.
#. Thus, if you want to place two :cyan:`Camp` waypoints closely together, make sure that the waypoint stats of the first one have disappeared from your HUD before you set the second one. If the stats of the first waypoint are still visible, you will accidentally modify the Camp start and Camp end markers of that waypoint instead of inserting a new point.
#. Don't place :cyan:`Camp` waypoints in strategically irrelevant areas, or you will see bots having a situation totally unimportant areas while their team mates are under heavy attack.
#. Provide the "defending" team with some nice sniper spots near the map goal! In general, if you make team-specific :cyan:`Camp` waypoints, make more for the defending team than for the attacking team.


Map Goal waypoints
~~~~~~~~~~~~~~~~~~~

This waypoint type obviously indicates the :purple:`Map Goal`.

On an As_ map, the :purple:`Map Goal` waypoint tells the bots where the VIP escape zone is. Make sure the escape zone symbol is visible on your HUD when you place a map goal waypoint there. Otherwise the VIP may end up reaching the point and running away again just like you would do with :white:`Rescue` waypoints.
On a Cs_ map, the :purple:`Map Goal` waypoint marks the position of the hostages. It is :redbold:`NOT` necessary to place one :purple:`Map Goal` waypoint per hostage. Unless the hostages are standing really far away from each other, one point per hostage group will perfectly do.
On a De_ map, the :purple:`Map Goal` waypoint marks the bomb spots. It must be placed somewhere inside the bomb zone, i.e. the bomb icon must be blinking on your HUD when you place such a waypoint. In contrast to Cs_ maps, on De_ maps it makes sense to set various goal waypoints in one bomb zone. This will enable bots to choose from several spots to plant the bomb and make them less predictable.

.. note:: On Es_ maps, the :purple:`Map Goal` waypoint marks the escape zone for the Terrorists. You can follow the same rules as for As_ maps. Now you may wonder how to determine the exact function of a the :purple:`Map Goal` waypoint. Don't worry, this is entirely map specific, you don't have to do anything about it. All bots of both participating teams will automatically know what the the :purple:`Map Goal` is, they only need the point to guide them there. You can add this type of waypoint by selecting ``8. Goal`` from the "\ :green:`Waypoint Type`\ " menu. :purple:`The colour of Goal waypoints is purple`, as you can see in the pictures below.

.. figure:: images/hostage_goal_point.png
    :align: center

    Hostage Goal Waypoint


.. figure:: images/vip_goal_point.png
    :align: center

    VIP Escape Goal Waypoint

.. figure:: images/bombplace_goal_point.png
    :align: center

    Bombplace Goal Waypoint

.. figure:: images/terrorists_escape_goal_point.png
    :align: center

    Terrorists escape Goal Waypoint (map es_trinity)


Jump Connections
~~~~~~~~~~~~~~~~~

:red:`Jump` is not actually a waypoint but a connection between waypoints. (see: Waypoint Connections)

So now we've seen what type of waypoints the bots use, we can see how we can string these waypoints together to make a giant web to cover the map with.

The Radius
---------------------------------

The :blue:`Radius` of a waypoint is indicated by blue polygon that go out in all directions from the position of a waypoint. The photo below was taken from above, we are looking down onto a waypoint, and you can see :blue:`the colour of a Radius is Blue`, as you can see in the picture below.

.. figure:: images/wayzone_radius.png
    :align: center

    Waypoint Radius